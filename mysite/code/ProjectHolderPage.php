<?php

class ProjectHolderPage extends Page {

}

class ProjectHolderPage_Controller extends Page_Controller {
	
    private static $allowed_actions = array (
        'languages',
        'tags'
    );

    public function languages(SS_HTTPRequest $request) {
        $language = ScriptLanguage::get()->byID($request->param('ID'));

        if(!$language) {
            return $this->httpError(404, 'That language could not be found');    
        }

        return array (
            'ScriptLanguage' => $language
        );
    }

    public function tags(SS_HTTPRequest $request) {
        $tag = Tag::get()->byID($request->param('ID'));

        if(!$tag) {
            return $this->httpError(404,'That tag could not be found');
        }

        return array (
            'Tag' => $tag
        );
    }
}