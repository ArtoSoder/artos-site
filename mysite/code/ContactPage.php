<?php

class ContactPage extends Page {

}

class ContactPage_Controller extends Page_Controller {
	private static $allowed_actions = array('Form');

	public function Form() {
		$fields = new FieldList(
			TextField::create('Name'),
			EmailField::create('Email'),
			TextareaField::create('Message')
		);

		$actions = new FieldList(
			FormAction::create('submit','Submit')
		);

		return new Form($this, 'Form', $fields, $actions);
	}

	public function submit($data, $form) {
		$email = new Email();

		$email->setTo('arto_sode@hotmail.com');
		$email->setFrom($data['Email']);
		$email->setSubject("Contact message from {$data["Name"]}");

		$messageBody = "
			<p><strong>Name:</strong> {$data['Name']}</p>
			<p><strong>Message:</strong> {$data['Message']}</p>
			";

		$email->setBody($messageBody);
		$email->send();
		return array(
			'Content' => '<p> Thank you for contacting me. </p>',
			'Form' => ''
		);

	}
}