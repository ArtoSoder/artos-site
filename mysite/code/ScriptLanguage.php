<?php

class ScriptLanguage extends DataObject {
	private static $db = array(
		'LanguageName' => 'Varchar(50)',

	);

	private static $belongs_many_many = array(
		'ProjectPages' => 'ProjectPage',
	);


	private static $summary_fields = array(
		'LanguageName' => 'Language'
	);

    public function Link() {
        return $this->ProjectPages()->First()->Parent()->Link('languages/' . $this->ID);
    }
}
