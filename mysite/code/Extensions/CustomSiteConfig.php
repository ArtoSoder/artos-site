<?php

class CustomSiteConfig extends DataExtension {

	private static $db = array(
		'AboutMe' => 'Text',
		'FacebookLink' => 'Varchar(255)',
		'TwitterLink' => 'Varchar(255)',
		'SteamLink' => 'Varchar(255)',
	);

	private static $has_one = array(
		'MyImage' => 'Image'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab('Root.Main', TextareaField::create('AboutMe', 'About me text'));
		$fields->addFieldToTab('Root.Main', TextField::create('FacebookLink','Link to Facebook'));
		$fields->addFieldToTab('Root.Main', TextField::create('TwitterLink','Link to Twitter'));
		$fields->addFieldToTab('Root.Main', TextField::create('SteamLink','Link to Steam'));
		$fields->addFieldToTab('Root.Main', UploadField::create('MyImage','Image of Me'));
	}
}