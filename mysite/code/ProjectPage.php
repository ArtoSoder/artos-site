<?php

class ProjectPage extends Page {

	private static $db = array(
		'ProjectName' => 'Varchar(255)',
		'ProjectDescription' => 'Text',
		'ProjectStatus' => "Enum('Finished,Ongoing,To-start,Postponed,Cancelled','Ongoing')"
	);

	private static $has_one = array(
		'ProjectIcon' => 'Image',
	);

	private static $many_many = array(
		'ScriptLanguages' => 'ScriptLanguage',
		'Tags' => 'Tag',
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->addFieldToTab('Root.Main', TextField::create('ProjectName','Project Name'));
		$fields->addFieldToTab('Root.Main', TextareaField::create('ProjectDescription','Description'));
		$fields->addFieldToTab('Root.Main', DropdownField::create('ProjectStatus','Status',
			singleton('ProjectPage')->dbObject('ProjectStatus')->enumValues()));
		$fields->addFieldToTab('Root.Main', UploadField::create('ProjectIcon','Project Image/icon'));

		$fields->insertAfter(Tab::create('Tags'),'Main');

		$config = GridFieldConfig_RelationEditor::create();
		$config->getComponentByType('GridFieldAddExistingAutocompleter')->setSearchFields(array('TagName'))->setResultsFormat('$TagName');
		$fields->addFieldToTab('Root.Tags', GridField::create(
			'Tags',
			'Tag',
			$this->Tags(),
			$config
		));

		$fields->insertAfter(Tab::create('Languages'),'Tags');

		$config = GridFieldConfig_RelationEditor::create();
		$config->getComponentByType('GridFieldAddExistingAutocompleter')->setSearchFields(array('LanguageName'))->setResultsFormat('$LanguageName');
		$fields->addFieldToTab('Root.Languages', GridField::create(
			'ScriptLanguages',
			'ScriptLanguage',
			$this->ScriptLanguages(),
			$config
		));
		return $fields;
	}
}

class ProjectPage_Controller extends Page_Controller {
	
}