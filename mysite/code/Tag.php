<?php

class Tag extends DataObject {
	private static $db = array(
		'TagName' => 'Varchar(50)',

	);

	private static $belongs_many_many = array(
		'ProjectPages' => 'ProjectPage',
		//'BlogArticles' => 'BlogArticle'
	);

	private static $summary_fields = array(
		'TagName' => 'Tag Name'
	);

    public function Link() {
        return $this->ProjectPages()->First()->Parent()->Link('tags/' . $this->ID);
    }
}

