<?php

class HomePage extends Page {

	private static $db = array(
		'BannerTitle' => 'Varchar(255)',
		'BannerText' => 'Varchar(255)'
	);

	private static $has_one = array(
        'BannerImage' => 'Image'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
        $fields->removeByName('Content');
		$fields->addFieldToTab('Root.Main',TextField::create('BannerTitle','Banner Title'));
		$fields->addFieldToTab('Root.Main',TextareaField::create('BannerText','Banner Text'));
        $fields->addFieldToTab('Root.Main',UploadField::create('BannerImage','Banner Image'));
		return $fields;
	}
}

class HomePage_Controller extends Page_Controller {

}