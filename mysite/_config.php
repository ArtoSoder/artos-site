<?php

global $project;
$project = 'mysite';
FulltextSearchable::enable();

global $database;
$database = '';

require_once('conf/ConfigureFromEnv.php');

// Set the site locale
i18n::set_locale('en_US');